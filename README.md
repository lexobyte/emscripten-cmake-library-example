# CMake/Emscripten library example
A simple demo of building a C++ library with CMake for both Emscripten and
traditional target platforms.

## Building
### Native build
As usual:

    build_dir=build/native
    mkdir -p $build_dir
    cmake -S . -B $build_dir
    make --directory $build_dir

Then to see the library in action:

    $build_dir/src/consumer

### Emscripten build
Use `emcmake` and `emmake` (`emmake` may not be necessary here but is still
advised by the Emscripten docs):

    build_dir=build/emscripten
    mkdir -p $build_dir
    emcmake cmake -S . -B $build_dir
    emmake make --directory $build_dir

Then to see the library in action:

    $build_dir/src/consumer.js

This build differs from the native build in the following ways.

-   The `mylib` library is actually declared to CMake using `add_executable`.
    In practice this was the easiest way to export a usable, standalone JS
    library.
-   The `mylib` library includes an extra source file, `js-api.cpp`, which
    declares the JS bindings.
-   Since Emscripten requires C++11 or above, the this is set as a compile
    feature for `mylib`.
-   Rather than building `consumer/consumer`, we copy a `consumer.js` script to
    the build directory.
