#include <emscripten/bind.h>

#include "mylib/mylib.h"


EMSCRIPTEN_BINDINGS(js_api) {
  emscripten::function("add", &mylib::add);
  emscripten::function("subtract", &mylib::subtract);
}
