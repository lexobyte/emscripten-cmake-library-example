#!/usr/bin/env node
const lib = require('../mylib/mylib');


// The functions exposed in mylib aren't ready immediately, so we have to wait
// for onRuntimeInitialized (which we set below) to be called. Wrapping this in
// a promise forces the Node runtime to wait.
(new Promise((resolve, reject) => {
  lib.onRuntimeInitialized = resolve;
}))
.then(() => {
  console.log('2 + 2 =', lib.add(2, 2));
})
.catch((err) => {
  console.error('Error:', err.message);
  console.error(err.stack);
});
